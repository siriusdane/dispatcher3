# coding=utf-8
"""
.. module:: BaseModules.Logger
    :platform: Independent
    :synopsis: Module for the logging of the project
"""
import datetime
import settings


def insert_record(module, function, message, message_type):
    """
    Function that inserts a record in the Log for the application
    :param module: Module from which the message comes from
    :param function: Name of the function doing the logging
    :param message: Message to be logged
    :param message_type: Type of the logging message (ERROR, INFO)
    :return:
    """
    collection = settings.mongodb_connect(settings.configuration().get('mongodb').get('logging'),
                                          settings.APPLICATION)
    post = {'module': module,
            'function': function,
            'message': message,
            'message_type': message_type,
            'timestamp': datetime.datetime.utcnow()}
    collection.insert(post)

