# coding=utf-8
"""
.. module:: BaseModules.Petition
    :platform: Independent
    :synopsis: Class Petition
"""
from BaseModules.Exceptions import PException


class Petition(object):
    """
    .. class:: Petition
    Class that defines the structure of the Petition and can store the data that is received
    from Back End as well as to serve as base for the data returned to Back End after processing
    the petition.

    """
    # Attributes
    id = None                   #: ID for the petition
    brand = None                #: Product Brand issuing the petition
    status = None               #: Status of the petition
    issuer = None               #: Information about the Issuer associated to the petition
    user = None                 #: Information about the User associated to the petition
    content = None              #: Information relevant to the resource associated to the petition
    license = None              #: Information about the License associated to the petition

    def __init__(self, petition):
        self.load(petition)

    def load(self, petition):
        self.id = petition.get('PetitionID')
        self.brand = petition.get('Brand')
        self.status = petition.get('Status')
        self.issuer = Issuer(petition.get('Issuer', {}))
        self.user = User(petition.get('User', {}))
        self.content = Content(petition.get('Content', {}))
        self.license = License(petition.get('License', {}))

    def validate(self):
        if not self.id:
            raise PException(400, 'Missing Petition Id')
        if not self.brand:
            raise PException(400, 'Missing Petition Brand')
        self.issuer.validate()
        self.user.validate()
        self.content.validate()

    def to_dict(self):
        return {'PetitionID': self.id,
                'Brand': self.brand,
                'Status': self.status,
                'FulfillmentMethod': 'F',
                'Issuer': self.issuer.to_dict(),
                'User': self.user.to_dict(),
                'Content': self.content.to_dict(),
                'License': self.license.to_dict(),
                'Institution': {'name': None,
                                'level': None}}


class Issuer(object):
    """
    .. class:: Issuer
    Class to be used by the Petition class that contains information about the
    issuer of the content in the petition
    """
    # ATTRIBUTES
    issuer_name = None      #: Name of the Issuer
    drm_type = None         #: DRM Type used by the Issuer

    def __init__(self, issuer):
        self.load(issuer)

    def load(self, issuer):
        self.issuer_name = issuer.get('IssuerType')
        self.drm_type = issuer.get('DRMType')

    def validate(self):
        if not self.issuer_name:
            raise PException(400, 'Missing Issuer name')
        if not self.drm_type:
            raise PException(400, 'Missing DRM Type')

    def to_dict(self):
        return {'IssuerType': self.issuer_name,
                'IssuerID': None,
                'secret_key': None,
                'issuer_url': None,
                'DRMType': self.drm_type}


class User(object):
    """
    .. class:: User
    Class to be used by the Petition class that contains information about the
    user to which the petition is assigned
    """
    # ATTRIBUTES
    id = None               #: ETC ID for the User
    username = None         #: Credential Name/Id
    password = None         #: Credential Key/Password

    def __init__(self, user):
        self.load(user)

    def load(self, user):
        self.id = user.get('etcID')
        self.username = user.get('userID')
        self.password = user.get('userKey')

    def validate(self):
        if not self.id:
            raise PException(400, 'Missing User Id')

    def to_dict(self):
        return {'etcID': self.id,
                'FirstName': None,
                'LastName': None,
                'userPwd': None,
                'access_token': None,
                'deviceID': None,
                'Country': None,
                'userKey': self.password,
                'userID': self.username}


class Content(object):
    """
    .. class:: Content
    Class to be used by the Petition class that contains information about the
    content to be assigned in the petition
    """
    # ATTRIBUTES
    token_path = None           #: Path to the requested Token
    file_path = None            #: Path to the requested File
    transaction_id = None       #: ID of the transaction associated to the Resource
    urn = None                  #: URN associated to the Resource
    ean13 = None                #: EAN13 Code associated to the Resource
    proprietary_code = None     #: Code of the resource in a proprietary format

    def __init__(self, content):
        self.load(content)

    def load(self, content):
        self.token_path = content.get('TokenPath')
        self.file_path = content.get('FilePath')
        self.transaction_id = content.get('TransactionID')
        self.urn = content.get('Urn')
        self.ean13 = content.get('ean13')
        self.proprietary_code = content.get('proprietaryCode')

    def validate(self):
        if not (self.urn or self.ean13 or self.proprietary_code):
            raise PException(400, 'Missing Product Identifier')

    def to_dict(self):
        return {'TokenPath': self.token_path,
                'FilePath': self.file_path,
                'BookID': None,
                'Title': None,
                'Publisher': None,
                'TransactionID': self.transaction_id,
                'Urn': self.urn,
                'ean13': self.ean13,
                'proprietaryCode': self.proprietary_code}


class License(object):
    """
    .. class:: License
    Class to be used by the Petition class that contains information about the
    license to be assigned to the content in the petition
    """
    # ATTRIBUTES
    date = None     #: Date until which the License will be valid
    days = None     #: Number of days to assign to the License

    def __init__(self, data):
        self.load(data)

    def load(self, data):
        self.date = data.get('date')
        self.days = data.get('days')

    def to_dict(self):
        return {'date': self.date,
                'type': None,
                'days': self.days}
