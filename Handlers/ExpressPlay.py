# coding=utf-8
"""
.. module:: .ExpressPlay
    :platform: Independent
    :synopsis: Module that will handle petitions for ExpressPlay content
"""
from BaseModules.Exceptions import PException
from BaseModules import Aspects
import requests
import json
import settings

license_service = None


def initialize_variables():
    globals()['license_service'] = settings.configuration().get('services').get('license')


class ExpressPlay(object):
    """
    .. class: ExpressPlay
    The handler class that is used to process a petition for an ExpressPlay content.
    """
    petition = None     #: Petition to be processed

    def __init__(self, petition):
        self.petition = petition

    @Aspects.exception_handler
    def check_order(self):
        """
        Method that consults if a link has been generated for a given transaction_id if
        it's provided, else it generates the transaction_id and returns False
        :return: Whether or not a link has already been generated for the transaction_id
        """
        if self.petition.content.transaction_id:
            headers = {'content-type': 'application/json'}
            params = {'web_service': 'ExpressPlay',
                      'web_method': 'check_order',
                      'transaction_id': self.petition.content.transaction_id}
            response = requests.post(license_service, data=json.dumps(params), headers=headers)
            if response.status_code == 200 and response.content:
                link = response.content.get('order_link')
                self._process_link(link)
                return True
            elif response.status_code == 400:
                raise PException(400, response.content)
            elif response.status_code != 200:
                raise PException(405, 'Error in response from ExpressPlay service.\n %d: %s' % (response.status_code,
                                                                                                response.content))
            return False
        else:
            self._create_transaction_id()
            return False

    @Aspects.exception_handler
    def get_license(self):
        """
        Method that asks for a license to a given product and provides the transaction_id
        for future reference.
        :return:
        """
        headers = {'content-type': 'application/json'}
        params = {'web_service': 'ExpressPlay',
                  'web_method': 'get_license',
                  'product_id': self.petition.content.urn,
                  'user_id': self.petition.user.username,
                  'user_key': self.petition.user.password,
                  'transaction_id': self.petition.content.transaction_id}
        response = requests.post(license_service, data=json.dumps(params), headers=headers)
        if response.status_code == 200 and response.content:
            link = response.content.get('order_link')
            self._process_link(link)
        elif response.status_code == 400:
            raise PException(400, response.content)
        else:
            raise PException(405, 'Error in response from ExpressPlay service.\n %d: %s' % (response.status_code,
                                                                                            response.content))

    def _process_link(self, link):
        """
        Given a link it process it and stores the link in the appropriate field in the
        petition class
        :param link: Link to the content requested
        :return:
        """
        self.petition.content.token_path = link
        self.petition.status = settings.UPLOADED

    def _create_transaction_id(self):
        """
        Method that creates a transaction_id for the given petition
        :return:
        """
        self.petition.content.transaction_id = '%s_%s_%s' % (self.petition.user.id, self.petition.id,
                                                             self.petition.content.urn[-5:])
