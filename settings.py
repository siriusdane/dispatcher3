# coding=utf-8
"""
.. module:: settings
    :platform: Independent
    :synopsis: Settings for the project
"""
import pymongo
import sys


# STATUSES
TO_PROCESS = 'D'
REPROCESS = 'I'
UPLOADED = 'U'
PENDING = 'P'


# GENERAL CONFIGURATION
ENVIRONMENT = 'LOCAL'
APPLICATION = None


# MONGODB CONFIGURATION
mongodb_host = 'localhost'
mongodb_port = 27017
mongodb_configuration = 'configuration'


# CONFIGURATION FUNCTIONS
def start_configuration():
    load_configuration()
    globals()['RUNNING'] = True
    set_application_status(True)
    globals()['RELOAD'] = False
    set_reload_status(False)
    globals()['DEBUG'] = False
    set_debug_status(False)


def load_configuration():
    collection = mongodb_connect(mongodb_configuration, APPLICATION)
    globals()['CONFIGURATION'] = collection.find_one({'environment': ENVIRONMENT})
    if not globals()['CONFIGURATION']:
        response = raw_input('No configuration was found in the DataBase.\n'
                             'Do you want to run with local configuration? (Y/n): ')
        if response.lower() != 'n':
            from Utilities import LocalConfiguration
            LocalConfiguration.application_configuration(ENVIRONMENT)
            globals()['CONFIGURATION'] = collection.find_one({'environment': ENVIRONMENT})
        else:
            sys.exit(0)
    set_reload_status(False)


# STATUS THREAD FUNCTION
def update_statuses():
    import time
    while running():
        globals()['RUNNING'] = get_application_status()
        globals()['DEBUG'] = get_debug_status()
        globals()['RELOAD'] = get_reload_status()
        time.sleep(30)


# AUXILIARY FUNCTIONS
def set_application_status(status):
    statuses = configuration().get('mongodb').get('statuses')
    collection = mongodb_connect(statuses, APPLICATION)
    collection.update({'environment': ENVIRONMENT}, {'$set': {'running': status}}, upsert=True)


def get_application_status():
    statuses = configuration().get('mongodb').get('statuses')
    collection = mongodb_connect(statuses, APPLICATION)
    status = collection.find_one({'environment': ENVIRONMENT})
    return status.get('running')


def set_debug_status(status):
    statuses = configuration().get('mongodb').get('statuses')
    collection = mongodb_connect(statuses, APPLICATION)
    collection.update({'environment': ENVIRONMENT}, {'$set': {'debugging': status}}, upsert=True)


def get_debug_status():
    statuses = configuration().get('mongodb').get('statuses')
    collection = mongodb_connect(statuses, APPLICATION)
    status = collection.find_one({'environment': ENVIRONMENT})
    return status.get('debugging')


def set_reload_status(status):
    statuses = configuration().get('mongodb').get('statuses')
    collection = mongodb_connect(statuses, APPLICATION)
    collection.update({'environment': ENVIRONMENT}, {'$set': {'reload': status}}, upsert=True)


def get_reload_status():
    statuses = configuration().get('mongodb').get('statuses')
    collection = mongodb_connect(statuses, APPLICATION)
    status = collection.find_one({'environment': ENVIRONMENT})
    return status.get('reload')


def set_application_name(name):
    globals()['APPLICATION'] = name


def mongodb_connect(name, collection):
    client = pymongo.MongoClient(mongodb_host, mongodb_port)
    db = client[name]
    return db[collection]


def mongodb_dropdb(name):
    client = pymongo.MongoClient(mongodb_host, mongodb_port)
    client.drop_database(name)


def debug():
    return globals().get('DEBUG')


def running():
    return globals().get('RUNNING')


def reload_configuration():
    return globals().get('RELOAD')


def configuration():
    return globals().get('CONFIGURATION', {})


def adobe_service():
    return globals().get('adobe_service')


def expressplay_service():
    return globals().get('expressplay_service')

