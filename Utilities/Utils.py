# coding=utf-8
"""
.. module:: Utilities.Utils
    :platform: Independent
    :synopsis: Utilities for the application
"""
from BaseModules.Exceptions import PException
import requests
import imp
import os

HANDLERS = 'Handlers'


# MAIN UTILITIES
# --------------
def obtain_issuer(petition):
    """
    Function that returns an instance of the Class that will be handling the petition
    :param petition: Petition to be processed
    :return: Instantiated class that will handle the petition
    """
    try:
        (fp, path, data) = imp.find_module(HANDLERS, [os.path.dirname(__file__) + '\..'])
        issuer_name = get_issuer_name(petition.issuer)
        handlers = imp.load_module(HANDLERS, fp, path, data)
        module = getattr(handlers, issuer_name)
        module_class = getattr(module, issuer_name)
        instantiated_class = module_class(petition)
        return instantiated_class
    except:
        raise PException(400, 'Incorrect Issuer Name')


def obtain_link_type(link):
    response = requests.head(link)
    while 'location' in response.headers:
        response = requests.head(response.headers['location'])
    content_type = response.headers.get('content-type')
    if content_type in ['application/pdf', 'application/epub+zip', 'application/vnd.adobe.adept+xml',
                        'application/vnd.marlin.drm.actiontoken+xml']:
        return content_type
    else:
        raise PException(400, 'Unexpected file type %s' % content_type)


def obtain_s3_path(petition, content_type):
    if content_type == 'application/pdf':
        extension = '.pdf'
    elif content_type == 'application/epub+zip':
        extension = '.epub'
    else:
        raise PException(400, 'Invalid file type')
    return str(petition.brand) + '/' + str(petition.user.id) + '/' + str(petition.id) + extension


# AUXILIARY FUNCTIONS
# -------------------
def get_issuer_name(issuer):
    """
    Function that returns the name of the Controller that should handle the petition
    :param issuer: Issuer part of the Petition class
    :return: Name of the controller that should handle the petition
    """
    issuer_name = issuer.issuer_name.lower()
    if issuer_name == 'ebooks':
        return 'Ebooks'
    elif issuer_name == 'libranda':
        return 'Libranda'
    elif issuer_name == 'not defined':
        drm_type = issuer.drm_type.lower()
        if drm_type == 'adobe':
            return 'EditionGuard'
        elif drm_type == 'expressplay':
            return 'ExpressPlay'
        raise PException
    raise PException