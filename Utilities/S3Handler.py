# coding=utf-8
"""
.. module:: Utilities.S3Handler
    :platform: Independent
    :synopsis: Perform interactions with the S3 Amazon Repository
"""
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import settings


aws_root = None
aws_access_key = None
aws_secret_key = None
aws_bucket = None


def initialize_variables():
    globals()['aws_root'] = settings.configuration().get('amazon').get('root')
    globals()['aws_bucket'] = settings.configuration().get('amazon').get('bucket')
    globals()['aws_access_key'] = settings.configuration().get('amazon').get('access_key')
    globals()['aws_secret_key'] = settings.configuration().get('amazon').get('secret_key')


def s3_upload_file(path, content, mime):
    initialize_variables()
    connection = S3Connection(aws_access_key, aws_secret_key)
    bucket = connection.get_bucket(aws_bucket)
    key = Key(bucket)
    key.key = path
    key.content_type = mime
    key.set_contents_from_string(content, policy='public-read')
    return key.generate_url(expires_in=0, query_auth=False, force_http=True)


def s3_delete_file(path):
    initialize_variables()
    connection = S3Connection(aws_access_key, aws_secret_key)
    bucket = connection.get_bucket(aws_bucket)
    key = Key(bucket)
    key.key = path
    if key.exists():
        key.delete()
    return None